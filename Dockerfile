FROM python:3.6-slim

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /code
ADD . /code/
RUN cp src/app/.env.docker src/app/.env

RUN pip install --upgrade pip
RUN pip install -r src/requirements.txt