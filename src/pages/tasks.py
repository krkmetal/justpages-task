from app import celery_app
from pages.models import Page


@celery_app.task
def increment_page_content_views(page_id):
    page = Page.objects.get(pk=page_id)
    page.increment_views_for_content()
