from django.db import models
from django.db.models import F
from polymorphic.models import PolymorphicModel


class Page(models.Model):
    title = models.CharField(max_length=150)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    def increment_views_for_content(self):
        self.contents.update(counter=F('counter') + 1)

    def add_text(self, title, body):
        content = Text.objects.create(
            page=self,
            title=title,
            body=body,
        )
        return content

    def add_audio(self, title, url):
        content = Audio.objects.create(
            page=self,
            title=title,
            url=url,
        )
        return content

    def add_video(self, title, url):
        content = Video.objects.create(
            page=self,
            title=title,
            url=url,
        )
        return content


class PageSortable(Page):
    """
    Proxy model for doubling admin for Page model.
    """
    class Meta:
        proxy = True


class Content(PolymorphicModel):
    page = models.ForeignKey(
        Page,
        related_name='contents',
        on_delete=models.CASCADE
    )
    counter = models.PositiveIntegerField(default=0)
    title = models.CharField(max_length=200)
    position = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta:
        ordering = ['position']

    def __str__(self):
        return self.title

    @property
    def item_type(self):
        return self.__class__.__name__.lower()

    @property
    def item(self):
        return 'temporary'


class Text(Content):
    body = models.TextField()


class Audio(Content):
    url = models.URLField()


class Video(Content):
    url = models.URLField()
