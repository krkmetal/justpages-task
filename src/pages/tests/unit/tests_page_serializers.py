import pytest
from rest_framework.request import Request
from rest_framework.test import APIRequestFactory

from pages.api.serializers import PageDetailSerializer, PageListSerializer
from pages.models import Text

pytestmark = pytest.mark.django_db


@pytest.fixture
def request_context():
    factory = APIRequestFactory()
    request = factory.get('/')
    serializer_ctx = {
        'request': Request(request),
    }
    return serializer_ctx


def test_page_list_serializer_one_page(page, request_context):
    serializer = PageListSerializer(page, context=request_context)

    got = serializer.data

    assert got['title'] == page.title
    assert got['url'] == f'http://testserver/api/v1/pages/{page.pk}/'


def test_page_detail_serializer_one_page(page):
    serializer = PageDetailSerializer(page)

    got = serializer.data

    assert got['title'] == page.title
    assert got['id'] == page.id
    assert len(got['contents']) == 0


def test_page_detail_serializer_one_page_with_five_content_items(page, mixer):
    for i in range(5):
        Text.objects.create(page=page, title=f'Text N{i}')

    serializer = PageDetailSerializer(page)

    got = serializer.data

    assert len(got['contents']) == 5
