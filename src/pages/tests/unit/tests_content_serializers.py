import pytest

from pages.api.serializers import AudioSerializer, ContentSerializer, TextSerializer, VideoSerializer
from pages.models import Audio, Text, Video

pytestmark = pytest.mark.django_db


def test_text_content_serializer(page):
    text = Text.objects.create(page=page, body='Text body')

    got = TextSerializer(text).data

    assert got['body'] == 'Text body'


def test_audio_content_serializer(page):
    audio = Audio.objects.create(page=page, url='http://example.com/audio.mp3')

    got = AudioSerializer(audio).data

    assert got['url'] == 'http://example.com/audio.mp3'


def test_video_content_serializer(page):
    video = Video.objects.create(page=page, url='http://example.com/video.mp4')

    got = VideoSerializer(video).data

    assert got['url'] == 'http://example.com/video.mp4'


def test_content_serializer_text_item(page):
    content = Text.objects.create(
        page=page,
        title='Text title',
        body='Text body',
    )

    got = ContentSerializer(content).data

    assert got['id'] == content.pk
    assert got['item_type'] == 'text'
    assert got['title'] == 'Text title'
    assert got['body'] == 'Text body'
    assert got['counter'] == 0


def test_content_serializer_audio_item(page):
    content = Audio.objects.create(
        page=page,
        title='Audio title',
        url='http://example.com/audio.mp3',
    )

    got = ContentSerializer(content).data

    assert got['id'] == content.pk
    assert got['item_type'] == 'audio'
    assert got['title'] == 'Audio title'
    assert got['url'] == 'http://example.com/audio.mp3'
    assert got['counter'] == 0


def test_content_serializer_video_item(page):
    content = Video.objects.create(
        page=page,
        title='Video title',
        url='http://example.com/video.mp4',
    )

    got = ContentSerializer(content).data

    assert got['id'] == content.pk
    assert got['item_type'] == 'video'
    assert got['title'] == 'Video title'
    assert got['url'] == 'http://example.com/video.mp4'
    assert got['counter'] == 0
