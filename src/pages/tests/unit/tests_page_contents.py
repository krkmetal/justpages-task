import pytest

from pages.models import Audio, Text, Video

pytestmark = pytest.mark.django_db


def tests_add_text_to_the_page(page):
    Text.objects.create(page=page, body='Page text')

    got = page.contents

    assert got.count() == 1
    assert got.first().body == 'Page text'


def tests_add_audio_to_the_page(page):
    Audio.objects.create(page=page, url='http://example.com/audio.mp3')

    got = page.contents

    assert got.count() == 1
    assert got.first().url == 'http://example.com/audio.mp3'


def tests_add_video_to_the_page(page):
    Video.objects.create(page=page, url='http://example.com/video.mp4')

    got = page.contents

    assert got.count() == 1
    assert got.first().url == 'http://example.com/video.mp4'


def tests_content_item_string_is_title(page, mixer):
    Text.objects.create(page=page, title='Text title')

    got = str(page.contents.first())

    assert got == 'Text title'
