import pytest

pytestmark = pytest.mark.django_db


def tests_page_add_text(page):
    page.add_text(title='Text title', body='Text body')

    got = page.contents.first()

    assert got.body == 'Text body'


def tests_page_add_audio(page):
    page.add_audio(title='Audio title', url='http://example.com/audio.mp3')

    got = page.contents.first()

    assert got.url == 'http://example.com/audio.mp3'


def tests_page_add_video(page):
    page.add_video(title='Video title', url='http://example.com/video.mp4')

    got = page.contents.first()

    assert got.url == 'http://example.com/video.mp4'
