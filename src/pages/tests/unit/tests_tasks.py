from unittest.mock import patch

import pytest
from rest_framework.reverse import reverse

pytestmark = pytest.mark.django_db


@pytest.fixture(autouse=True)
def eager_celery(settings):
    settings.CELERY_ALWAYS_EAGER = True


def tests_increment_content_views_task_single_time(page, api):
    with patch('pages.tasks.increment_page_content_views.delay') as _task:
        url = reverse('v1:page-detail', kwargs={'pk': page.pk})

        api.get(url)

        assert _task.call_count == 1
        assert _task.call_args[0][0] == page.pk


def tests_increment_content_views_task_ten_times(page, api):
    with patch('pages.tasks.increment_page_content_views.delay') as _task:
        url = reverse('v1:page-detail', kwargs={'pk': page.pk})

        for i in range(10):
            api.get(url)

        assert _task.call_count == 10
