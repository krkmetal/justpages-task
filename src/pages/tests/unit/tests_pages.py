import pytest

from pages.models import Text

pytestmark = pytest.mark.django_db


def test_page_title(mixer):
    page = mixer.blend('pages.Page', title='Page title')

    got = page.title

    assert got == 'Page title'


def test_page_string_is_page_title(mixer):
    page = mixer.blend('pages.Page', title='Something')

    got = str(page)

    assert got == 'Something'


@pytest.mark.freeze_time('2025-05-15 15:25:35')
def test_page_creation_time(page):
    got = page.created

    assert got.year == 2025
    assert got.month == 5
    assert got.day == 15
    assert got.hour == 15
    assert got.minute == 25
    assert got.second == 35


def test_page_increment_views_counter_for_content(page):
    for i in range(10):
        Text.objects.create(page=page, title='Some title')
    page.increment_views_for_content()

    got = page.contents.all()

    assert len(got) == 10
    for content in got:
        assert content.counter == 1
