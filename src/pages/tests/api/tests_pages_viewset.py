import pytest
from rest_framework.reverse import reverse

pytestmark = pytest.mark.django_db


def tests_page_viewset_list_empty(api):
    url = reverse('v1:page-list')

    got = api.get(url).data

    assert got['count'] == 0
    assert len(got['results']) == 0


def tests_page_viewset_list_three_pages(api, many_pages):
    many_pages(3)
    url = reverse('v1:page-list')

    got = api.get(url).data

    assert got['count'] == 3
    assert len(got['results']) == 3
    assert got['next'] is None


def tests_page_viewset_list_15_pages_with_pagination(api, many_pages):
    many_pages(25)
    url = reverse('v1:page-list')

    got = api.get(url).data

    assert got['count'] == 25
    assert len(got['results']) == 10
    assert '/pages/?page=2' in got['next']


def tests_page_viewset_list_single_page_data(api, mixer):
    mixer.blend('pages.Page', title='Test api', id=1)
    url = reverse('v1:page-list')

    got = api.get(url).data['results']

    assert len(got) == 1
    assert got[0]['title'] == 'Test api'
    assert got[0]['url'] == 'http://testserver/api/v1/pages/1/'


def tests_page_viewset_detail_page_data(api, mixer):
    mixer.blend('pages.Page', title='Detail page', id=15)
    url = reverse('v1:page-detail', kwargs={'pk': 15})

    got = api.get(url).data

    assert got['title'] == 'Detail page'
    assert got['id'] == 15
    assert len(got['contents']) == 0
