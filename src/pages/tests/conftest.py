import pytest
from mixer.backend.django import mixer as _mixer
from rest_framework.test import APIClient


@pytest.fixture
def page(mixer):
    return mixer.blend('pages.Page')


@pytest.fixture
def many_pages(mixer):
    def create_pages(qty):
        return mixer.cycle(qty).blend('pages.Page')
    return create_pages


@pytest.fixture
def mixer():
    return _mixer


@pytest.fixture
def api():
    return APIClient()
