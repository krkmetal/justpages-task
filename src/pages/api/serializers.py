from rest_framework import serializers

from pages.models import Audio, Content, Page, Text, Video


class PageListSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='v1:page-detail')

    class Meta:
        model = Page
        fields = [
            'url',
            'title',
        ]


class PageDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Page
        fields = [
            'id',
            'title',
            'created',
            'modified',
            'contents',
        ]

    def to_representation(self, obj):
        representation = super().to_representation(obj)
        representation.pop('contents')
        items = obj.contents.all()

        if all(map(lambda it: isinstance(it, Content), items)):
            items_representation = ContentSerializer(items, many=True).data
        elif len(items) == 0:
            items_representation = []
        else:
            raise Exception('Unexpected type of content item')

        representation['contents'] = items_representation

        return representation


class TextSerializer(serializers.ModelSerializer):
    class Meta:
        model = Text
        fields = [
            'item_type',
            'body',
        ]


class AudioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Audio
        fields = [
            'item_type',
            'url',
        ]


class VideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Video
        fields = [
            'item_type',
            'url',
        ]


class ContentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Content
        fields = [
            'id',
            'title',
            'counter',
            'item'
        ]

    def to_representation(self, obj):
        representation = super().to_representation(obj)
        representation.pop('item')
        if obj.item_type == 'text':
            item_representation = TextSerializer(obj).data
        elif obj.item_type == 'audio':
            item_representation = AudioSerializer(obj).data
        elif obj.item_type == 'video':
            item_representation = VideoSerializer(obj).data
        else:
            raise Exception('Unexpected type of content item')

        for key in item_representation:
            representation[key] = item_representation[key]

        return representation
