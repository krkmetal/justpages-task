from rest_framework import viewsets
from rest_framework.pagination import PageNumberPagination

from pages import tasks
from pages.api.serializers import PageDetailSerializer, PageListSerializer
from pages.models import Page


class PageListPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 50


class PageViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Page.objects.order_by('id').all()
    pagination_class = PageListPagination

    def get_serializer_class(self):
        if self.action == 'list':
            return PageListSerializer
        else:
            return PageDetailSerializer

    def retrieve(self, request, *args, **kwargs):
        response = super(PageViewSet, self).retrieve(request, *args, **kwargs)
        page_id = self.get_object().pk
        tasks.increment_page_content_views.delay(page_id)
        return response
