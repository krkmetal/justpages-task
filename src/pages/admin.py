from django.contrib import admin
from grappelli.forms import GrappelliSortableHiddenMixin
from polymorphic.admin import (
    PolymorphicChildModelAdmin, PolymorphicChildModelFilter, PolymorphicInlineSupportMixin,
    PolymorphicParentModelAdmin, StackedPolymorphicInline)

from pages.models import Audio, Content, Page, PageSortable, Text, Video


class ContentInline(StackedPolymorphicInline):
    class TextContentInline(StackedPolymorphicInline.Child):
        model = Text
        fields = ('title', 'body',)

    class AudioContentInline(StackedPolymorphicInline.Child):
        model = Audio
        fields = ('title', 'url')

    class VideoContentInline(StackedPolymorphicInline.Child):
        model = Video
        fields = ('title', 'url')

    model = Content

    child_inlines = (
        TextContentInline,
        AudioContentInline,
        VideoContentInline,
    )


@admin.register(Page)
class PageAdmin(PolymorphicInlineSupportMixin, admin.ModelAdmin):
    inlines = (ContentInline, )
    list_display = ('__str__', 'id')
    list_per_page = 15
    ordering = ('id',)
    search_fields = (
        'title',
        'contents__title',
    )


@admin.register(Content)
class ContentAdmin(PolymorphicParentModelAdmin):
    child_models = (Text, Audio, Video)
    list_filter = (PolymorphicChildModelFilter,)
    list_display = ('__str__', 'item_type')
    list_per_page = 15
    search_fields = ('title', )
    polymorphic_list = True


@admin.register(Text)
class ContentAdmin(PolymorphicChildModelAdmin):
    readonly_fields = ('counter', 'position')


@admin.register(Audio)
class ContentAdmin(PolymorphicChildModelAdmin):
    readonly_fields = ('counter', 'position')


@admin.register(Video)
class ContentAdmin(PolymorphicChildModelAdmin):
    readonly_fields = ('counter', 'position')


class SortableContentInline(GrappelliSortableHiddenMixin, admin.TabularInline):
    fields = ('title', 'position')
    model = Content
    extra = 0
    can_delete = False

    def has_add_permission(self, request):
        return False


@admin.register(PageSortable)
class PageOrderingAdmin(admin.ModelAdmin):
    inlines = (SortableContentInline, )
    list_display = ('__str__', 'id')
    list_per_page = 15
    ordering = ('id',)

    def has_add_permission(self, request):
        return False
