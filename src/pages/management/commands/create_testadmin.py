from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

TEST_ADMIN_NAME = 'testadmin'
TEST_ADMIN_EMAIL = 'testadmin@example.com'
TEST_ADMIN_PASSWORD = 'Pass123'


class Command(BaseCommand):
    help = 'Crate a superuser testadmin for testing purposes'

    def handle(self, *args, **options):
        UserModel = get_user_model()

        try:
            UserModel._default_manager.db_manager('default').get_by_natural_key(TEST_ADMIN_NAME)
            print("Testadmin already exists.")
        except UserModel.DoesNotExist:
            user = UserModel._default_manager.db_manager('default').create_superuser(
                username=TEST_ADMIN_NAME,
                email=TEST_ADMIN_EMAIL,
                password=TEST_ADMIN_PASSWORD
            )
            user.save()
            print("Testadmin created successfully.")
