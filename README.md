# JustPages

## Описание

Просто страницы с различным видом контента.

## Запуск

```sh
git clone https://gitlab.com/krkmetal/justpages-task
cd justpages-task
docker-compose build

# запуск тестов
docker-compose up autotests

# запуск сервера
docker-compose up runserver
```

При запуске миграций загружается начальный набор данных, наполненный случайными значениями.

## Использование

При запуске `runserver` для доступа к админке создается админ с именем `testadmin` и паролем `Pass123`. 

Доступ к админке `http://127.0.0.1:8000/admin/`.


В админке есть три страницы для работы со страницами (раздел `Pages`):
  - `Contents` для добавления контента к страницам 
  - `Page sortables` для изменение порядка отображения контента в детализации страницы
  - `Pages` для добавления страниц с возможностью добавлять/изменять контент в виде inline блоков

Для чтения страниц есть два API эндпоинта (доступны без авторизации):

 - `GET /api/v1/pages/` для списка страниц с пагинацией(`?page=<page_number>`, а также количества объектов на странице `?page_size=<size>`, изначально установлено 10, максимально 50) 
 - `GET /api/v1/pages/<page_id>/` для детализации страницы.

## Примеры использования API

Пример ответа на запрос списка страниц `http://127.0.0.1:8000/api/v1/pages/`:

```json
{
  "count":25,
  "next":"http://127.0.0.1:8000/api/v1/pages/?page=2",
  "previous":null,
  "results":[
    {
      "url":"http://127.0.0.1:8000/api/v1/pages/1/",
      "title":"Simple page"
    },
    {
      "url":"http://127.0.0.1:8000/api/v1/pages/2/",
      "title":"Another page"
    },
    {
      "url":"http://127.0.0.1:8000/api/v1/pages/3/",
      "title":"And yet another page"
    },
    ...
  ]
 }
```


Пример ответа на запрос о детальной информации по странице `http://127.0.0.1:8000/api/v1/pages/1/`:

```json
{
  "id":1,
  "title":"Simple page",
  "created":"2018-08-20T18:06:23.877594Z",
  "modified":"2018-08-23T09:56:28.726781Z",
  "contents":[
    {
      "id":1,
      "title":"Text 1",
      "counter":5,
      "item_type":"text",
      "body":"Body of the first text"
    },
    {
      "id":2,
      "title":"Audio 1",
      "counter":5,
      "item_type":"audio",
      "url":"http://example.com/audio1.mp3"
    },
    {
      "id":3,
      "title":"Audio 2",
      "counter":4,
      "item_type":"audio",
      "url":"http://example.com/audio2.mp3"
    },
    {
      "id":4,
      "title":"Video 1",
      "counter":3,
      "item_type":"video",
      "url":"http://example.com/video1.mp4"
    },
    ...
  ]
 }
```